import {Component} from "angular2/core";
import {ProfileService} from "./profile.service";
import {Profile} from "./profile";
import {WeatherService} from "./weather/weather.service";
import {WeatherItem} from "./weather/weather";
import {OnInit} from "angular2/core";

@Component({
    selector: "my-sidebar",
    template: `
        <h3>Saved Profiles</h3>
        <button (click)="onSaveNew()">Save List to Profile</button>
        <article class="profile" *ngFor="#profile of profiles" (click)="onLoadProfile(profile)">
            <h4>{{profile.profileName}}</h4>
            <p>Cities: {{profile.cities.join(", ") }}</p>
            <span class="delete" (click)="onDeleteProfile($event, profile)">X</span>
        </article>
    `,
    styleUrls: ['src/css/sidebar.css'],
    providers: [ProfileService, WeatherService]
})

export class SidebarComponent implements  OnInit{
    profiles: Profile[];
    private profileService: ProfileService;
    private weatherService: WeatherService;

    constructor(profileService: ProfileService, weatherService: WeatherService){
        this.profileService = profileService;
        this.weatherService = weatherService;
    }

    ngOnInit() {
        this.profiles = this.profileService.getProfiles()
    }

    onSaveNew () {
        const cities = this.weatherService.getWeatherItems().map(function (element: WeatherItem) {
            return element.cityName;
        });
        if (cities.length > 0) this.profileService.saveNewProfile(cities);

    }

    onLoadProfile(profile: Profile) {
        this.weatherService.clearWeatherItems();
        for (let i = 0; i < profile.cities.length; i ++) {
            this.weatherService.searchWeatherData(profile.cities[i])
                .retry()
                .subscribe(
                  data => {
                      const weatherItem = new WeatherItem(data.name, data.weather[0].description, data.main.temp);
                      this.weatherService.addWeatherItem(weatherItem);
                  }
                );
        }
    }

    onDeleteProfile (event: Event, profile: Profile) {
        event.stopPropagation();
        this.profileService.deleteProfile(profile);
    }


}