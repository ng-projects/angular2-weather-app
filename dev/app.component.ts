import {Component} from 'angular2/core';
import {WeatherListComponent} from "./weather/weather-list.component";
import {WeatherSearchComponent} from "./weather/weather.search.component";
import {SidebarComponent} from "./sidebar.component";

@Component({
    selector: 'my-app',
    templateUrl: "dev/app.component.html", 
    directives: [WeatherListComponent, WeatherSearchComponent, SidebarComponent]

})
export class AppComponent {
}