///<reference path="../../node_modules/rxjs/Observable.d.ts"/>
import {Component} from 'angular2/core';
import {ControlGroup} from 'angular2/common';
import {WeatherService} from './weather.service';
import {WeatherItem} from './weather';
import {Subject} from 'rxjs/Subject';

@Component({
    selector: "weather-search",
    templateUrl: "dev/weather/weather.search.component.html",
    providers: [WeatherService]
})
export class WeatherSearchComponent implements OnInit {

    private weatherService : WeatherService;
    private searchStream = new Subject<string>();
    private data : any = {};

    constructor(weatherService : WeatherService){
        this.weatherService = weatherService;
    }

    onSubmit() {
        const weatherItem = new WeatherItem(this.data.name, this.data.weather[0].description, this.data.main.temp);
        this.weatherService.addWeatherItem(weatherItem);

    }

    onSearchLocation (cityName: string) {
        //is like emitting to the stream listener
        this.searchStream
            .next(cityName);
    }

    ngOnInit() {
        //setup listener to subject searchStream
        this.searchStream
            .debounceTime(400)
            .distinctUntilChanged()
            .switchMap(r => {
                if (r) {
                    console.log("R exists: ", r);
                    return this.weatherService.searchWeatherData(r);
                }

                else return {};
            })
            .subscribe(
                (data: Response) => {
                    console.log("data: ", data);
                    this.data = data;
                },

                (error : Response) => {
                    console.log("ERRORRR: ", error)
                }
            )


    }
}