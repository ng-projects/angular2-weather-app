var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("angular2/core");
var weather_data_1 = require("./weather.data");
var Observable_1 = require("rxjs/Observable");
require("rxjs/Rx");
var WeatherService = (function () {
    function WeatherService(http) {
        this.api = "http://api.openweathermap.org/data/2.5/weather";
        this.apiKey = "4b480b9291d053d334b43d7cc3c4435b";
        this.http = http;
    }
    WeatherService.prototype.getWeatherItems = function () {
        return weather_data_1.WEATHER_ITEMS;
    };
    WeatherService.prototype.addWeatherItem = function (weatherItem) {
        weather_data_1.WEATHER_ITEMS.push(weatherItem);
    };
    WeatherService.prototype.clearWeatherItems = function () {
        weather_data_1.WEATHER_ITEMS.splice(0);
    };
    WeatherService.prototype.searchWeatherData = function (cityName) {
        return this.http.get(this.api + "?q=" + cityName + "&APPID=" + this.apiKey + "&units=metric")
            .map(function (response) {
            var data = response.json();
            if (data.name)
                return data;
            else
                return {};
        })
            .catch(function (err) {
            console.log("###ERR", err);
            return Observable_1.Observable.throw(err.json());
        });
    };
    WeatherService = __decorate([
        core_1.Injectable()
    ], WeatherService);
    return WeatherService;
})();
exports.WeatherService = WeatherService;
//# sourceMappingURL=weather.service.js.map