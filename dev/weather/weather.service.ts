import  {Injectable} from "angular2/core";
import {Http, Response} from "angular2/http";
import {WEATHER_ITEMS} from "./weather.data";
import {Observable} from "rxjs/Observable";
import "rxjs/Rx";
import {WeatherItem} from "./weather";

@Injectable()
export class WeatherService {
    private http:Http;
    private api:string = "http://api.openweathermap.org/data/2.5/weather";
    private apiKey:string = "4b480b9291d053d334b43d7cc3c4435b";

    constructor(http:Http) {
        this.http = http;
    }

    getWeatherItems() {
        return WEATHER_ITEMS
    }

    addWeatherItem(weatherItem:WeatherItem) {
        WEATHER_ITEMS.push(weatherItem);
    }

    clearWeatherItems() {
        WEATHER_ITEMS.splice(0);
    }

    searchWeatherData(cityName:string):Observable<Response> {
        return this.http.get(this.api + "?q=" + cityName + "&APPID=" + this.apiKey + "&units=metric")
            .map(response => {
                let data = response.json();
                if (data.name) return data;
                else return {}
            })
            .catch(err => {
                console.log("###ERR", err);
                return Observable.throw(err.json());
            });


    }

}






