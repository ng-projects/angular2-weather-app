var WeatherItem = (function () {
    function WeatherItem(cityName, desc, temperature) {
        this.cityName = cityName;
        this.desc = desc;
        this.temperature = temperature;
    }
    return WeatherItem;
})();
exports.WeatherItem = WeatherItem;
//# sourceMappingURL=weather.js.map