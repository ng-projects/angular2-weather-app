var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
///<reference path="../../node_modules/rxjs/Observable.d.ts"/>
var core_1 = require('angular2/core');
var weather_service_1 = require('./weather.service');
var weather_1 = require('./weather');
var Subject_1 = require('rxjs/Subject');
var WeatherSearchComponent = (function () {
    function WeatherSearchComponent(weatherService) {
        this.searchStream = new Subject_1.Subject();
        this.data = {};
        this.weatherService = weatherService;
    }
    WeatherSearchComponent.prototype.onSubmit = function () {
        var weatherItem = new weather_1.WeatherItem(this.data.name, this.data.weather[0].description, this.data.main.temp);
        this.weatherService.addWeatherItem(weatherItem);
    };
    WeatherSearchComponent.prototype.onSearchLocation = function (cityName) {
        //is like emitting to the stream listener
        this.searchStream
            .next(cityName);
    };
    WeatherSearchComponent.prototype.ngOnInit = function () {
        var _this = this;
        //setup listener to subject searchStream
        this.searchStream
            .debounceTime(400)
            .distinctUntilChanged()
            .switchMap(function (r) {
            if (r) {
                console.log("R exists: ", r);
                return _this.weatherService.searchWeatherData(r);
            }
            else
                return {};
        })
            .subscribe(function (data) {
            console.log("data: ", data);
            _this.data = data;
        }, function (error) {
            console.log("ERRORRR: ", error);
        });
    };
    WeatherSearchComponent = __decorate([
        core_1.Component({
            selector: "weather-search",
            templateUrl: "dev/weather/weather.search.component.html",
            providers: [weather_service_1.WeatherService]
        })
    ], WeatherSearchComponent);
    return WeatherSearchComponent;
})();
exports.WeatherSearchComponent = WeatherSearchComponent;
//# sourceMappingURL=weather.search.component.js.map