export class WeatherItem {
    cityName : string;
    desc: string;
    temperature: number;

    constructor (cityName: string, desc: string, temperature: number) {
        this.cityName = cityName;
        this.desc = desc;
        this.temperature = temperature;
    }
}