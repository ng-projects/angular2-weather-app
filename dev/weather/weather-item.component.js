var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("angular2/core");
var WeatherItemComponent = (function () {
    function WeatherItemComponent() {
    }
    __decorate([
        core_1.Input("item")
    ], WeatherItemComponent.prototype, "weatherItem");
    WeatherItemComponent = __decorate([
        core_1.Component({
            selector: "weather-item",
            templateUrl: "dev/weather/weather-item.component.html",
            styleUrls: ["src/css/weather-item.css"]
        })
    ], WeatherItemComponent);
    return WeatherItemComponent;
})();
exports.WeatherItemComponent = WeatherItemComponent;
//# sourceMappingURL=weather-item.component.js.map