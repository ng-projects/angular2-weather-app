import {Component, Input} from "angular2/core";
import {WeatherItem} from "./weather";

@Component({
    selector: "weather-item",
    templateUrl: "dev/weather/weather-item.component.html",
    styleUrls: ["src/css/weather-item.css"],
    // inputs: ["weatherItem: item"]
})
export class WeatherItemComponent {
    //replacing inputs at @Component declaration
    @Input("item") weatherItem : WeatherItem;
}