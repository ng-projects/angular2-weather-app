import {Component} from "angular2/core";
import {OnInit} from "angular2/core";
import {WeatherItemComponent} from "./weather-item.component";
import {WeatherItem} from "./weather";
import {WEATHER_ITEMS} from "./weather.data";
import {WeatherService} from "./weather.service";

@Component({
    selector: "weather-list",
    templateUrl: "dev/weather/weather-list.component.html",
    directives: [WeatherItemComponent],
    providers: [WeatherService]
})
export class WeatherListComponent implements OnInit {
    weatherItems: WeatherItem[];
    weatherService: WeatherService;

    constructor(weatherService: WeatherService) {
        this.weatherService = weatherService;
    }


    ngOnInit(): any {
        this.weatherItems = WEATHER_ITEMS;
    }


}